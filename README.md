# AdmiosFlixWeb project
 
# Description
 
This project is the UI to show movies catalog.

 
#### Table of Contents
 
- [Frontend](#frontend)
	- [Installation](#installation)
	- [Quick Start](#quickstart)
	- [Usage](#usage)
	- [Docker](#docker)
	- [Docs](#docs)
	- [What is included](#whatisincluded)
- [Server](#server)


 
# Frontend
 
#### Installation
- [Install nvm](https://github.com/nvm-sh/nvm) in order to use different versions of node

``` bash
# clone the repo
$ git clone https://gitlab.com/joseluisceron13/admiosflixweb.git
 
# go into app's directory
$ cd admiosflixweb
 
# use node version 12.13.0
$ nvm install 12.13.0
$ nvm use 12.13.0
 
# install app's dependencies
$ npm install
```
 
#### Quick Start

- [Download the .env.local file](https://drive.google.com/file/d/1mxWkjdnaphagPhCt5HdfVfwBCYf_wTc9/view?usp=sharing) and place it in `./` folder

```
admiosflixweb
├── .env.local         # environment variables file
├── src/            
└── package.json
...
```

#### Usage
 
``` bash
# serve at localhost:8080 with automatic restart when saving
npm run serve
 
# Compiles and minifies for production
npm run build
 

```
user: tester@gmail.com
pass: 123456

#### Docker
 
``` bash
# clone image
docker pull dockerjctest1/vue_docker
 
# run 
docker run -p 8000:8080 dockerjctest1/vue_docker

```

Keep in mind that this component points to a server so that componente need to be deployed fist.

 
#### What is included

Within the download you will find the following directories and files, logically grouping common assets and providing both compiled and minified variations. You will see something like this:

```
admiosflixweb
├── public/          # static files
│   └── index.html   # html template
│
├── src/             # project root
│   ├── assets/      # images, icons, styles, etc.
│   ├── components/  # common components - header, footer, sidebar, etc.
│   ├── containers/     # The container of components
│   ├── plugins/       # bootstrap imports
│   └── router/       # paths of the application
│   └── store_modules/       # Vuex State files
│   ├── views/       # application views
│   ├── App.vue
│   ├── store.js   # main state management file
│   └── main.js
│
└── package.json
```

## Server
- Go to this [repository](https://gitlab.com/joseluisceron13/admiosflixserver.git) to execute the server project
- Have in mind that the server project is required in order to run the frontend component. Because the frontend project sends the petitions to it

## Features and technologies

- bootstrap: front end css library
- firebase auth: allows multiplatform access 
- vuex: for state management
- vue-resourse: for http requests
- async functions: Async functions return a Promise by default, so you can rewrite any callback based function to use Promises, then await their resolution



**Table of Contents**

[TOCM]

[TOC]

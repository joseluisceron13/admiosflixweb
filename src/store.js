import Vue from "vue";
import Vuex from "vuex";

// modules
import loginModule from "./store_modules/login/index";
import movieModule from "./store_modules/movies/index";
Vue.use(Vuex);

const modules = {
  login: loginModule,
  movie: movieModule,
};


export default new Vuex.Store({
  modules,
});

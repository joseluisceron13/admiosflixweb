export default {
  defMovies(state, movies) {
    state.movies = movies;
  },
  defShowDetail(state, showDetail) {
    state.showDetail = showDetail
  },
};

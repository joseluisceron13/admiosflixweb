import movieActions from "./actions.js";
import movieGetters from "./getters.js";
import movieMutations from "./mutations.js";

export default {
  namespaced: true,
  state(){
    return {
      movies: [],
      showDetail: false,
    }
  },
  mutations: movieMutations,
  actions: movieActions,
  getters: movieGetters,
};

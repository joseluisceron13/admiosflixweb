export default {
  movies(state) {
    return state.movies;
  },
  showDetail(state) {
    return state.showDetail;
  }
};

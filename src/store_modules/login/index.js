import loginActions from "./actions.js";
import loginGetters from "./getters.js";
import loginMutations from "./mutations.js";

export default {
  namespaced: true,
  state(){
    return {
      userId: null
    }
  },
  mutations: loginMutations,
  actions: loginActions,
  getters: loginGetters,
};

import "@babel/polyfill";
import "mutationobserver-shim";
import Vue from "vue";
import "./plugins/bootstrap-vue";
import App from "./App.vue";
import router from "./router";
import firebase from "firebase";
import store from './store'
import VueResource from 'vue-resource';
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';
import moment from 'moment';
import Vuelidate from "vuelidate";

Vue.use(VueResource);
// For icons
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
moment.locale('en');
Vue.use(Vuelidate)

Vue.filter('dateFormat', function(value) {
  if (value != '' && value != null) {    
    return moment.utc(value).format('DD-MMM-YYYY')
  }else{
    return 'Indefinido'
  }
});

Vue.config.productionTip = false;

Vue.prototype.$log = console.log.bind(console)

var app = '';
// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: process.env.VUE_APP_FIREBASE_API_KEY,
  authDomain: process.env.VUE_APP_FIREBASE_AUTH_DOMAIN,
  projectId: process.env.VUE_APP_FIREBASE_PROJECT_ID,
  storageBucket: process.env.VUE_APP_FIREBASE_STORAGE_BUCKET,
  messagingSenderId: process.env.VUE_APP_FIREBASE_MESSAGING_SENDER_ID,
  appId: process.env.VUE_APP_FIREBASE_APP_ID,
}

// Initialize Firebase
firebase.initializeApp(firebaseConfig);
Vue.prototype.$firebase = firebase;

firebase.auth().onAuthStateChanged((user) => {   
  if (!app) {
      app = new Vue({
        el: '#app',
        router,
        store,            
        template: '<App/>',
        render: (h) => h(App),
      })
  }
})
import Vue from "vue";
import VueRouter from "vue-router";
import firebase from 'firebase';

// Containers
const TheContainer = () => import("@/containers/TheContainer");

// Views
const Dashboard = () => import("@/views/Dashboard");

// Views - Pages
const Page404 = () => import('@/views/pages/Page404');
const Login = () => import('@/views/pages/Login');

// Views - Movies
const CreateMovie = () => import('@/views/movies/Form');

Vue.use(VueRouter);

let routes = [
  {
    path: "/",
    redirect: "/dashboard",
    name: "Home",
    component: TheContainer,
    children: [
      {
        path: "dashboard",
        component: Dashboard,
        meta: {
          requiresAuth: true
        },   
      },
    ],
  },
  {
    path:"/pages",
    redirect:"/pages/404",
    name: "pages",
    component: TheContainer,
    children: [
      {
        path: 'login',
        name: 'Login',
        component: Login,
        meta: {
          noAuthpage: true
        }
      },
      {
        path: '404',
        name: 'Page404',
        component: Page404
      },
    ]
  },
  {
    path: "/movie",
    name: "movie",
    component: TheContainer,
    children: [
      {
        path: "create",
        name: "movie-create",
        component: CreateMovie,
        meta: {
          requiresAuth: true
        }, 
      }
    ]
  }
];

const router = new VueRouter({
  mode: "history",
  // base: process.env.BASE_URL,
  linkActiveClass: "open active",
  scrollBehavior: () => ({ y: 0 }),
  routes,
});

router.beforeEach(async (to, from, next) => {
  const currentUser = firebase.auth().currentUser
  const requiresAuth = to.matched.some(record => record.meta.requiresAuth)
  const noAuthpage = to.matched.some(record => record.meta.noAuthpage)
  const isCurrentUser = currentUser != null ? true : false
  const currentUserId = currentUser != null ? currentUser.uid : ''

  if (!isCurrentUser && requiresAuth){
    await firebase.auth().signOut()
    next({path: '/pages/login', replace: true})
  } else if (isCurrentUser && noAuthpage){
    next({path: '/dashboard', replace: true})   
  } else  {
    next()    
  }
});

export default router;
